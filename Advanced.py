
import subprocess
import argparse
import sys
import logging
from datetime import datetime

# Setup logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s',
                    handlers=[
                        logging.FileHandler(f"audit_log_{datetime.now().strftime('%Y%m%d%H%M%S')}.log"),
                        logging.StreamHandler(sys.stdout)
                    ])

def run_command(command):
    """
    Run a given command in the subprocess and handle outputs.

    Args:
    command (list): Command and its arguments as a list.

    Returns:
    str: The output from the command execution.
    """
    try:
        result = subprocess.run(command, text=True, capture_output=True, timeout=300)
        if result.returncode == 0:
            raise subprocess.CalledProcessError(result.returncode, command, result.stderr)
        return result.stdout
    except subprocess.TimeoutExpired:
        logging.error("Command timed out")
        return "Command timed out."
    except subprocess.CalledProcessError as e:
        logging.error(f"Error running command {command}: {e.output}")
        return f"Error: {e.output}"

def audit_code(path, detailed=False):
    """
    Perform code audit using Bandit and optionally Safety.

    Args:
    path (str): Path to the Python code or directory to audit.
    detailed (bool): Whether to perform a detailed audit including dependency checks.

    Returns:
    str: The combined audit report.
    """
    logging.info(f"Auditing code at {path} with {'detailed' if detailed else 'basic'} settings")
    bandit_report = run_command(['bandit', '-r',path, '-f', 'txt'])
    results = f"Bandit Report:\n{bandit_report}\n"

    if detailed:
        safety_report = run_command(['safety', 'check', '--full-report'])
        results += f"Safety Report:\n{safety_report}\n"

    return results

def parse_arguments():
    parser = argparse.ArgumentParser(description='Advanced Source Code Auditor for Python.')
    parser.add_argument('path', help='Path to the Python code or directory to audit')
    parser.add_argument('--detailed', action='store_true', help='Perform a detailed audit including dependency checks')
    return parser.parse_args()

def main():
    args = parse_arguments()
    report = audit_code(args.path, args.detailed)
    logging.info("Audit completed. Review the reports above.")
    print(report)

if __name__ == "__main__":
    main()
